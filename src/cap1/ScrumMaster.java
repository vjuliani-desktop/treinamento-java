package cap1;

public class ScrumMaster extends Funcionario {

    @Override
    public Double getSalarioCalculado() {
        return this.getSalario() + (this.getSalario() * 0.30);
    }
}
