package cap1;

public class Desenvolvedor extends Funcionario  {

    @Override
    public Double getSalarioCalculado() {
        return this.getSalario() + (this.getSalario() * 0.50);
    }
}
