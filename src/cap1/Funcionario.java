package cap1;

public class Funcionario {

    Funcionario() {}

    Funcionario(String nome, int codigo) {
        this.codigo = codigo;
        this.nome = nome;
    }


    private String nome;
    private Integer codigo;
    private double salario;
    private CargoEnum cargo;

    public CargoEnum getCargo() {
        return cargo;
    }

    public void setCargo(CargoEnum cargo) {
        this.cargo = cargo;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public double getSalario() {
        return this.salario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Double getSalarioCalculado() {
       return this.salario;
    }
}
